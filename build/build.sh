#!/bin/bash

rm ./public/*.html

for file in `find ./ -maxdepth 1 -name "*.md"`; do
   pandoc -f org -t html "$file" \
          -o "./public/`basename "$file" .md`.html"\
          -M document-css=false\
          --lua-filter ./build/updateLinks.lua \
          -s &
done

#-B ./build/header.html \
#-H ./build/includes.html \
