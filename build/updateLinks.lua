function Link(el)
  el.target = string.gsub(el.target, "%.org", ".html")
  el.target = string.gsub(el.target, "::%*", "#")
  return el
end
